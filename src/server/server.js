const http = require('http')
const readFile = require('./readFile')
const uuid4 = require('uuid4')
http.createServer((request, response)=>{

    let statusCode
    let delay

    if(request.url.search('/status/') == 0){
        statusCode = request.url.slice('/status/'.length)
        request.url = "/status/"
    } 
    if(request.url.search('/delay/') == 0){
        delay = request.url.slice('/delay/'.length)
        request.url = "/delay/"
    }   

    switch(request.url){
        case '/':{
            readFile('src/public/index.html')
            .then((data)=>{
                response.writeHead(200, {"Content-Type" : "text/html"})
                response.write(data)
                response.end()
            })
            .catch((error)=>{
                throw error
            })
            
        }
        break
        case '/style.css':{
            readFile('src/public/style.css')
            .then((data)=>{
                response.writeHead(200, {"Content-Type" : "text/css"})
                response.write(data)
                response.end()
            })
            .catch((error)=>{
                throw error
            })
            
        }
        break
        case '/index.js':{
            readFile('src/public/index.js')
            .then((data)=>{
                response.writeHead(200, {"Content-Type" : "text/js"})
                response.write(data)
                response.end()
            })
            .catch((error)=>{
                throw error
            })
            
        }
        break
        case '/data/data.json':{
            readFile('src/data/data.json')
            .then((data)=>{
                response.writeHead(200, {"Content-Type" : "application/json"})
                response.write(data)
                response.end()
            })
            .catch((error)=>{
                throw error
            })
            
        }
        break
        case '/status/':{
            response.writeHead(statusCode, {"Content-Type" : "text/html"})
            response.write(`
            <center>
                <h1>This response has status code of ${statusCode}</h1>
            </center>
            `)
            response.end()
        }
        break
        case '/delay/':{
            response.writeHead(200, {"Content-Type" : "text/html"})
            setTimeout(()=>{
                response.write(`
                    <center>
                        <h1>This response is delayed by ${delay} seconds</h1>
                    </center>
                `)
                response.end()
            },delay * 1000)
        }
        break
        case '/uuid':{
            response.writeHead(200, {"Content-Type" : "text/html"})
            response.write(`
                <center>
                    <h1>New Generated UUID4 : ${uuid4()}</h1>
                </center>
            `)
            response.end()
        }
        break
        case '/html':{
            readFile('src/public/sample.html')
            .then((data)=>{
                response.writeHead(200, {"Content-Type" : "text/html"})
                response.write(data)
                response.end()
            })
            .catch((error)=>{
                throw error
            })
        }
        break
        case '/json':{
            readFile('src/data/data.json')
            .then((data)=>{
                response.writeHead(200, {"Content-Type" : "application/json"})
                response.write(data)
                response.end()
            })
            .catch((error)=>{
                throw error
            })
            
        }
        break
        default:{
            response.writeHead(404, {"Content-Type" : "text/html"})
            response.write(`
                <center>
                    <h1>Page Not Found</h1>
                    <h2>Error Code 404</h2>
                </center>
            `)
            response.end()
        }



    }
})
.listen(8000)