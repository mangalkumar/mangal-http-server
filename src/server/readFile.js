const fs = require('fs')

function readFile(fileName){
    return new Promise((resolve, reject)=>{
        fs.readFile(fileName, 'utf-8', (error, data)=>{
            if(error){
                return reject(error)
            }

            return resolve(data)
        })  
    })
}

module.exports = readFile